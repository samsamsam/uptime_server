#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/sysinfo.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 

int main(int argc, char** argv)
{
   int sockfd = -1;
   int new_fd = -1;
   struct sockaddr_in my_addr;
   struct sockaddr_in their_addr;
   socklen_t sin_size;
   sin_size = sizeof(struct sockaddr_in);
   int yes=1;

   int service_port = 3490;
   int service_backlog = 10;

   if (argc != 3) {
      printf("uptime_server version 2021.12.11.00\n");
      printf("Usage: %s SERVICE_PORT BACKLOG\n\tfor example: %s 3490 10\n", argv[0], argv[0]);
      exit(EXIT_FAILURE);
   }

   service_port = atoi(argv[1]);
   service_backlog = atoi(argv[2]);

   // 0 means INADDR_ANY, we don't want it here
   if (service_port <= 0 || service_port > 0x10000) {
       fprintf(stderr, "Wrong service port value %d\n", service_port);
       exit(1);
   }

   if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      perror("socket");
      exit(1);
   }

   if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
      perror("setsockopt");
      exit(1);
   }

   my_addr.sin_family = AF_INET;
   my_addr.sin_port = htons((unsigned short)service_port);
   my_addr.sin_addr.s_addr = INADDR_ANY;
   memset(&(my_addr.sin_zero), '\0', 8);

   if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1) {
      perror("bind");
      exit(1);
   }

   if (listen(sockfd, service_backlog) == -1) {
      perror("listen");
      exit(1);
   }

   yes = 1;
   while (yes) {
      if ((new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size)) == -1) {
         perror("accept");
         continue;
      }

      struct timeval tv;
      tv.tv_sec = 0;
      tv.tv_usec = 100000; // 100ms
      setsockopt(new_fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv,sizeof(struct timeval));

      struct sysinfo s_info = {0};
      if(sysinfo(&s_info) != 0)
      perror("sysinfo");

      printf("server: got connection from %s, %ld\n", inet_ntoa(their_addr.sin_addr), s_info.uptime);
      char buf[10];
      ssize_t size;
      while ((size = read(new_fd, buf, sizeof(buf)-1)) > 0) {
         buf[size] = '\0';
         printf("%s", buf);
      }
      printf("\n");

      const char response[] = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
      dprintf(new_fd, "%s%ld", response, s_info.uptime);
      shutdown(new_fd, SHUT_WR);

      close(new_fd);

      sleep(1); //to limit requests  frequency
#ifdef TEST_BUILD
      if (access("/tmp/stop_uptime", F_OK) == 0) {
         yes = 0;
      }
#endif
   }
    close(sockfd);

    return 0;
}